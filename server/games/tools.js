
// let shuff = array => array.sort(() => Math.random()-.5) // I have to think about this ...
// let shuffle = array => shuff(shuff(shuff(shuff(shuff(shuff(shuff(shuff(shuff([...array]))))))))) // I'll think about it later

function shuffle(array) {
	tmp = array.map( (e,i) => [e,Math.random()])
	tmp = tmp.sort( (a,b) => a[1]-b[1] )
	return tmp.map( e => e[0])
}

function range(from, to) {
	if(to == undefined) {
		to = from
		from = 0
	}
	let r = Math.max(0,to-from)
	return new Array(r).fill().map((e, i) => from + i)
}

function properString(str) {return str.toLowerCase().replace(/[^0-9a-z_\-]/gi, '') }

function svg2html(url,width,height,style='') {
	return `<svg width="${width}" height="${height}" style="${style}"><image xlink:href="${url}" width="100%" height="100%"/></svg>`
}

exports.shuffle = shuffle
exports.range = range
exports.properString = properString
exports.svg2html = svg2html