let {range,shuffle} = require('./tools.js')

let clone = array => array.map(x=>x)
let orangify = str => '<orange>'+str+'</orange>'

class Default {

	isSpectator(user) {
		if(!user.name) return true
		return false
	}

	constructor(user) {
		this.start = user => {
			user.roommate.map( p => this.show(p))
			return true
		}

		this.show = user => user.execClient('gui',`
			<corona-time>
<punchline>It's
<orange>
╔══╦════╗╔═══════╗╔═══════╗ ╔════╤══╗╔═══╗ ╔══╗╔═══════╗
│ ╾╨╮   ││       ││   ╔═╗ │ │    ╰╼ ││   ╚╗║  ││  ╔═╗  │
│       ││    ┴┭╮││   ╚═╝ ╚╗│       ││    ╚╝  ││  ╚═╝  │
│     ╔═╝│  ╔═╗ ╿││   ╔══╗ ││  ╔═╗  ││  ╔╗    ││       │
│     ╚═╗│  ╚═╝ ┆││   ║  ║ ││  ╚═╝  ││  ║╚╗   ││  ╔═╗  │
╚═══╧═══╝╚══════╧╝╚═══╝  ╚═╝╚═══════╝╚══╝ ╚═══╝╚══╝ ╚══╝
╔═══════╗╔═══╗╔═══╗ ╔═══╗╔═══════╗
│       ││   ││   ╚═╝   ││   ╔═══╝
╚═╗   ╔═╝│   ││         ││   ╚══╗ 
  │   │  │ ┭╮││  ╔╗ ╔╗  ││   ╔══╝ 
  │   │  │  ╿││  ║╚═╝║  ││   ╚═══╗
  ╚═══╝  ╚══╧╝╚══╝   ╚══╝╚═══════╝
</orange></punchline>

</corona-time>
		`

// <div style="height:2em"></div>

// <div> featuring ${user.roommateplayer.length} player${user.roommateplayer.length<=1?'':'s'} </div>
// <grid style="width:max-content;grid-auto-flow:column;grid-gap:1em;padding:1em;margin:auto;">
// 	<div>${/*['h1llo','lsaof','fwg','ahrewgwgrw','rah','saf']*/user.roommateplayer.join('</div><div>')}</div>
// </grid>

// <div>──── select a game ────</div>
// <div style="height:2em"></div>

// <grid style="grid-template-columns:1fr 1fr 1fr;grid-gap: 5px;margin:auto;">
// 	<!--<input placeholder="random seed">-->
// 	<button onclick="r.execServer('timebomb.start')" style="font-family: 'Fishwrapper';font-size:20px">
// 		<div>${5} ≤ ${user.roommateplayer.length} ≤ ${10}</div>
// 		<div>Time Bomb</div>
// 	</button>
// 	<button onclick="r.execServer('secretNigon.start')" style="font-family: 'Eskapade';font-size:25px">
// 		<div>Secret Nigon</div>
// 	</button>
// 	<button onclick="r.execServer('coup.start')" style="font-family: 'EhmckeSchwabacher';font-size:23px">
// 		<div>ComplotS</div>
// 	</button>
// 	<button onclick="r.execServer('midi.start')" style="font-size:23px">
// 		<div>MIDI</div>
// 	</button>
// </grid>

// <div style="height:2em"></div>


		)
	}
}

exports.Default = Default
