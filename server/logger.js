const fs = require('fs')
const P = require('path')
const dateFormat = require('dateformat')
const col = require('./consoleColor.js')

let getTimeStr = () => dateFormat(new Date(), "yyyy-mm-dd h:MM:ss")
let getDateStr = () => dateFormat(new Date(), "yyyy-mm-dd")

let appendFile = (path,str) => {
	let dir = P.dirname(path)
	fs.mkdir(dir, {recursive:true}, e => {if(e) console.log(e)})
	fs.appendFile(path, str+'\n', e => {if(e) console.log(e)})
}

let log     = msg => console.log(`${col.y}${getTimeStr()}${col.no} ${msg}`)
let logFile = msg => appendFile(`logs/${getDateStr()}.log`, msg)
let err     = msg => console.log(`${col.r}${getTimeStr()}${col.no} ${msg}`)

exports.log = log
exports.logFile = logFile
exports.err = err
