
let GUI = id => {
	let out = document.getElementById(id)||{}
	out.toggle = (state,cssClass) => {
		cssClass = cssClass || 'hide'
		if(state == true)      document.getElementById(id).classList.remove(cssClass)
		if(state == false)     document.getElementById(id).classList.add(cssClass)
		if(state == undefined) document.getElementById(id).classList.toggle(cssClass)
	}
	out.scrollBottom = () => {
		document.getElementById(id).scrollTop = document.getElementById(id).scrollHeight;
	}
	out.get = () => document.getElementById(id)
	return out
}

let GUIInterfaceSetup = (name,initialState,shortcutKeyCode,callback,hideAfterInput) => {
	let state = initialState||false
	let localtoggle = force => {
		state = (force==undefined?!state:force)
		if(state) {
			GUI(name).toggle(!state,'activate')
			GUI(name+'-interface').toggle(state)
			if(GUI(name+'-input').focus) GUI(name+'-input').focus()
			if(GUI(name+'-input').select)GUI(name+'-input').select()
		} else {
			GUI(name).toggle(!state,'activate')
			GUI(name+'-interface').toggle(state)
		}
	}

	GUI(name).onclick = () => localtoggle()
	localtoggle(state)

	// console.log(name)
	if(GUI(name+'-input')){
		let f = (callbackEnabled=false) => {
			let x = GUI(name+'-input').value
			GUI(name+'-input').select()
			if(callbackEnabled && callback) callback(x)
			if(hideAfterInput) localtoggle(false)
		}
		GUI(name+'-input'    ).onchange = e => f()
		GUI(name+'-input'    ).onkeypress = e => {if(e.code=="Enter")f(true)}
		if(GUI(name+'-input-btn')) GUI(name+'-input-btn').onclick  = () => f(true)
	}

	if(shortcutKeyCode) {
		window.addEventListener('keydown',e => {
			// if(e.key=="Escape") {
			// 	localtoggle(false)
			// }
			if(e.ctrlKey && (e.keyCode==shortcutKeyCode || e.code==shortcutKeyCode)) {
				e.preventDefault()
				e.stopPropagation()
				localtoggle()
			}
		})
	}

	return localtoggle
}

