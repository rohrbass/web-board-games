"use strict"

class Socket {

	listener = {}
	listen(actionName,callback) {
		this.listener[actionName] = this.listener[actionName] || []
		this.listener[actionName].push(callback)
	}
	unlisten(actionName,callback) {
		// TODO
	}

	execServer(exec,args) {this.connection.send(JSON.stringify({exec:exec,args:args}))}

	constructor(socketAddress,connectCallback,connectionLostCallback) {
		this.connection = new WebSocket(socketAddress)

		if(connectCallback) this.connection.onopen = event => connectCallback(event)

		this.connection.onerror = error => console.log("connexion failed")

		setInterval(() => {if(this.connection.readyState !== 1) connectionLostCallback()}, 2000)

		this.connection.onmessage = msg => {
			msg = JSON.parse(msg.data)
			if(this.listener[msg.exec]) this.listener[msg.exec].map(callback => callback(msg.args))
		}
	}
}

function cookie(param) {
	let cook = {}
	document.cookie.split(/\s*;\s*/).forEach(pair => {
		let p = pair.split(/\s*=\s*/)
		cook[p[0]] = p.splice(1).join('=')
	})
	if(param) return cook[param]
	return cook
}

function jsonHighlight(json) {
	if (typeof json == 'string') {
		return json
	}
	json = JSON.stringify(json, undefined, 2)
	json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;')
	return '<JSON>' + json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
		let cls = 'number'
		if (/^"/.test(match)) {
			if (/:$/.test(match)) {
				cls = 'key'
			} else {
				cls = 'string'
			}
		} else if (/true|false/.test(match)) {
			cls = 'boolean'
		} else if (/null/.test(match)) {
			cls = 'null'
		}
		return `<${cls}>${match}</${cls}>`
	}) + '</JSON>'
}

let roomname = ""
let r = {}

////////////////////////////////////////////////////////////////////

let socket
{
	let connect_callback = () => {
		log(welcome)
		// get in room
		room(decodeURI(window.location.pathname.split('/')[2]))
		login(cookie("username"))
	}
	let connection_lost_callback = () => GUI("coronatime").classList.add("connection-lost")

	let URL = window.location.href.split('/')

	URL[0]='ws:'
	if(URL[3]!='room') console.log("won't work here")
	URL[3]='ws/room'

	let socketURL = URL.join('/')
	socket = new Socket(socketURL,connect_callback,connection_lost_callback)
}

////////////////////////////////////////////////////////////////////

let login = username => {
	socket.execServer("login",username||"")
}
socket.listen('return.login',username => {
	GUI('username').innerHTML = username
	GUI("login-input").value=username
	document.cookie="username="+username

	r.show()
})

let list = () => socket.execServer('list') // TODO make it a sync function
socket.listen('return.list' ,args => {log(args)})

////////////////////////////////////////////////////////////////////

let room = rn => {
	roomname = rn
	window.history.pushState({},'','/room/'+roomname)
	r.execServer('join')
	r.show()
	return r
}

r.execServer  = (exec,args) => socket.execServer('room',{room:roomname,exec:exec,args:args})

r.show  = () => r.execServer('show')

socket.listen("event.room.list", roommates => {
	// GUI('room-userlist').innerHTML = `[${roommates.join('][')}]`

	// roommates = ['marlene','pierre','robin','guillaume','ausra','rosine','adolf']
	// let out = {}
	// for(let k of roommates) {
	// 	if( out[k]==undefined ) out[k]=1
	// 	else out[k]++
	// }
	// let str= ''
	// for(let k in out) {
	// 	str += k
	// 	if(out[k]>1) {
	// 		str += `<span style="font-size:75%">[x${out[k]}]</span>`
	// 	}
	// 	str += ' '
	// }
	// GUI('room-userlist').innerHTML = `<span style="color:rgba(255,255,255,0.7)">${str}</span>`


	// GUI('room-userlist').innerHTML = `<span style="color:rgba(255,255,255,0.5)">${debug.join(' ')}</span>`
	GUI('room-usercount').innerHTML = roommates.length
	GUI('room-userlist').innerHTML = roommates.join('\n')
})

////////////////////////////////////////////////////////////////////

// socket.listen('event.room.userlist',args => {/*TODO*/})

////////////////////////////////////////////////////////////////////

let log = (msg,also_in_oneliner=false) => {
	if(!msg) return
	GUI('console-out').innerHTML += jsonHighlight(msg) + '\n'
	GUI('console-out').scrollBottom()
	if(!!also_in_oneliner) {
		GUI('log-out-oneliner').innerHTML = (GUI('log-out-oneliner').innerHTML + msg + '\n').split('\n').splice(-100).join('\n')
		GUI('log-out-oneliner').scrollBottom()
	}
}


document.addEventListener('keydown', e => {
	if(e.key=="Escape") {
		GUI('confirmation').style.display = "none"
	}
})
let confirmation = (callback,msg) => {
	if(!callback) return

	GUI('confirmation').style.display = "grid"
	GUI('confirmation').innerHTML = `
		<div style="margin:auto;width:max-content;background:url('/games/default/fabric-dark.png');padding:30px 50px;border-radius:5px;box-shadow:0 0 10px black">
			<h1>${msg?msg:"Are you sure ? This is dangerous."}</h1>
			<div style="height:30px"></div>
			<grid id='confirmation-yes-cancel' style="grid-template-columns:1fr 2fr;grid-gap:10px;margin:10px;">
			</grid>
		</div>
	`
	let yes = document.createElement('button')
	let cancel = document.createElement('button')
	yes.innerHTML = "YES"
	cancel.innerHTML = "CANCEL"
	yes.onclick    = () => { GUI('confirmation').style.display='none'; callback() }
	cancel.onclick = () => { GUI('confirmation').style.display='none' }
	let yes_cancel = document.getElementById('confirmation-yes-cancel')
	yes_cancel.appendChild(yes)
	yes_cancel.appendChild(cancel)

}

const audioCtx = new AudioContext()

function audioFromURL(url,volume = 1) {
	let source = audioCtx.createBufferSource()
	let request = new XMLHttpRequest()
	request.open('GET', url, true)
	request.responseType = 'arraybuffer'
	request.onload = function() {
		let audioData = request.response
		audioCtx.decodeAudioData(audioData, function(buffer) {
			// songLength = buffer.duration;

			let gainNode = audioCtx.createGain()
			gainNode.gain.value = volume
			gainNode.connect(audioCtx.destination)
			// source.connect(gainNode)

			source.buffer = buffer
			source.connect(gainNode)
			// source.connect(audioCtx.destination)
			// source.loop = true
		},
		function(e){"Error with decoding audio data" + e.error})
	}
	request.send()
	return source
}

// everytim we reevaluate this, we get a new pitch
function sfx_drama_pitch_sequencer() {
	let pitch = 0
	return () => {
		pitch += 5 + (Math.random()<0.5?(Math.random()<0.5?3:-8):0)
		if(pitch<-8) pitch+=12
		if(pitch>7) pitch-=12
		// pitch += 5
		// if(pitch>8) pitch-=12
		return pitch
	}
}

let sfx_mute = false
function toggleMute(setTo) {
	if(setTo== undefined) sfx_mute = !sfx_mute
	else sfx_mute = setTo

	if(sfx_mute) {
		GUI('mute-button').innerHTML = '<img src="/games/default/sound-off.png">'
	} else {
		GUI('mute-button').innerHTML = '<img src="/games/default/sound-on.png">'
	}
}
function sfx(url,pitch,volume=1) {
	if(sfx_mute==true) return

	if(pitch == undefined) {
		pitch = (Math.random()-0.5)*3
	} else if(typeof(pitch) == 'function') {
		pitch = pitch()
	}
	let a = audioFromURL(url,volume)
	a.start(0)
	// a.stop(0)
	a.playbackRate.value = Math.pow(Math.pow(2,1/12),pitch)
}

function random(fromArray) {
	if(fromArray == undefined) return Math.random()
	return fromArray[Math.floor(Math.random()*fromArray.length)]
}

socket.listen('gui' ,args => GUI('gui').innerHTML=jsonHighlight(args))
socket.listen('gui-top-one-liner' ,args => GUI('gui-top-one-liner').innerHTML=jsonHighlight(args))
socket.listen('log' ,args => log(args,true))
socket.listen('sfx' ,args => sfx(args))
socket.listen('wizz' ,args => {
	sfx('/games/default/msn-wizz.mp3')
	document.body.classList.remove('wizz')
	setTimeout( () => {
		document.body.classList.add('wizz')
	},100)
})

// socket.listen('return.room' ,args => {GUI('roomname').innerHTML = args;GUI("room-input").value=args})
// socket.listen('return.room' ,args => {GUI('roomname').innerHTML = args})

////////////////////////////////////////////////////////////////////
